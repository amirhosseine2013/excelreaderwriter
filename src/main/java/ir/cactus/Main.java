package ir.cactus;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Main {
    public static void main(String[] args) {

        XSSFWorkbook workbook =new XSSFWorkbook();

        XSSFSheet sheet=workbook.createSheet("Employee data");


        Map<String,Object[]> data=new TreeMap<String,Object[]>();
        data.put("1",new Object[]{"ID","NAME","LASTNAME"});
        data.put("2",new Object[]{1,"amirhossein","fardi"});
        data.put("3",new Object[]{2,"ali","fardi"});
        data.put("4",new Object[]{3,"abolfazl","hoj"});
        data.put("5",new Object[]{4,"leaila","ghafari"});
        data.put("6",new Object[]{5,"hossein","fardi"});
        data.put("7",new Object[]{6,"hossein","fardi"});
        data.put("8",new Object[]{7,"hossein","fardi"});


        Set<String> keySet =data.keySet();
        int rowNum=0;
        for (String key:keySet){
            Row row=sheet.createRow(rowNum++);
            Object []arr=data.get(key);
            int cellnum=0;
            for (Object obj:arr){
                Cell cell =row.createCell(cellnum++);
                if (obj instanceof String)
                    cell.setCellValue((String)obj);
                else if (obj instanceof Integer)
                    cell.setCellValue((Integer)obj);
            }
        }
        try{

            FileOutputStream fileOutputStream=new FileOutputStream(new File("employee_demo2.xlsx"));
            workbook.write(fileOutputStream);
            fileOutputStream.close();
            System.out.println("done");
        }catch (IOException e){
            e.printStackTrace();
        }


    }
}