package ir.cactus;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;

public class ReadFile {
    public static void main(String[] args) {

        try{
            FileInputStream fileInputStream=new FileInputStream(new File("name.xlsx"));

            XSSFWorkbook workbook=new XSSFWorkbook(fileInputStream);
            XSSFSheet sheet= workbook.getSheetAt(0);
            Iterator<Row> rowIterator=sheet.iterator();


            while (rowIterator.hasNext()){
                 Row row=rowIterator.next();

                 Iterator<Cell> cellIterator=row.cellIterator();
                  while (cellIterator.hasNext()){
                      Cell cell=cellIterator.next();
                      switch (cell.getCellType()){
                          case NUMERIC:
                              System.out.println(cell.getNumericCellValue()+"\t");
                              break;
                          case STRING:
                              System.out.println(cell.getStringCellValue()+"\t");
                              break;
                      }

                  }
                System.out.println(" ");
            }
            fileInputStream.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
